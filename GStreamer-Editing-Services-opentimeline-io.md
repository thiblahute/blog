# GStreamer Editing Services OpenTimelineIO support

[OpenTimelineIO] is an Open Source API and interchange format for editorial
timeline information, it basically allows some form of interoperability between
the different post production Video Editing tools. It is being developed by
[Pixar] and several other studios are contributing to the project allowing it to
[evolve quickly].

We, at [Igalia], recently [landed] support for the [GStreamer Editing Services]
(GES) serialization format in OpenTimelineIO, making it possible to convert GES
timelines to [any format supported] by the library. This is extremely useful to
integrate GES into existing Post production workflow as it allows projects in
any [format supported by OpentTimelineIO] to be used in the GStreamer Editing
Services and vice versa.

On top of that we are building a [GESFormatter] that allows us to transparently
handle any file format supported by OpenTimelineIO. In practice it will be
possible to use cuts produced by other video editing tools in any project using
GES, for instance the [Pitivi]:

<video src="https://gitlab.gnome.org/thiblahute/blog/raw/master/OpenTimelineIO/OpenTimelineIODemoScreencast.mov?inline=false#t=0.01" controls style="background-color: #050505; width: 512px; height: 216px"></video>

At [Igalia] we are aiming at making GStreamer ready to be used in existing Video
post production pipelines and this work is one step in that direction. We are
working on additional features in GES to fill the gaps toward that goal, for
instance we are now implementing [nested timeline] support and [framerate based
timestamps] in GES. Once we implement them, those features will enhance
compatibility of Video Editing projects created from other NLE softwares through
OpenTimelineIO. Stay tuned for more information!

[OpenTimelineIO]: https://github.com/PixarAnimationStudios/OpenTimelineIO
[Pixar]: http://graphics.pixar.com/
[evolve quickly]: https://github.com/PixarAnimationStudios/OpenTimelineIO/commits/master
[Igalia]: https://igalia.com
[landed]: https://github.com/PixarAnimationStudios/OpenTimelineIO/pull/41
[GStreamer Editing Services]: https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gstreamer-editing-services/html/ges-architecture.html
[any format supported]: https://opentimelineio.readthedocs.io/en/latest/tutorials/adapters.html
[format supported by OpentTimelineIO]: https://opentimelineio.readthedocs.io/en/latest/tutorials/adapters.html
[GESFormatter]: https://gitlab.freedesktop.org/gstreamer/gst-editing-services/merge_requests/67
[Pitivi]: http://www.pitivi.org
[Igalia]: https://www.igalia.com
[nested timeline]: https://gitlab.freedesktop.org/gstreamer/gst-editing-services/issues/60
[framerate based timestamps]: https://gitlab.freedesktop.org/gstreamer/gst-editing-services/issues/61
